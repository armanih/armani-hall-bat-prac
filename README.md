# README #

Simple contact management app allowing for creation and deletion of contacts.

#Setup

```npm install -g gulp bower nodemon```

```npm install mongoose --save```

```npm install body-parser --save```

```npm install underscore --save```

#To Start
node server/server.js

Navigate to 'http://localhost:7777/'

Hosted locally in the ```node``` branch, as the VM hosted Cassandra DB is not responding appropriately. That version can be found in the ```master``` branch.